package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;

@SpringBootApplication
@EnableZipkinStreamServer
@Slf4j
public class Zipkin {
    public static void main(String[] args) {
        log.info("start execute Zipkin....\n");
        SpringApplication.run(Zipkin.class, args);
        log.info("end execute Zipkin....\n");
    }
}